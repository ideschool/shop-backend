const express = require('express');
const AppRouter = require('./app-router');
const path = require('path');
const cors = require('cors');

class App {
    constructor() {
        this.app = express();
        this.startServer(3000).then(() => {
            console.log('App has been created. Go to localhost:3000/db/shop_name to get data');
        });
        this.routes();
    }

    routes() {
        const appRouter = new AppRouter();
        this.app.use('/db', cors(), appRouter.router);
        this.app.use('/static', express.static(path.join(__dirname, 'images')));
    }

    startServer(port) {
        return new Promise((resolve) => {
            this.app.listen(port, () => {
                resolve();
            });
        });
    }
}

const app = new App();
