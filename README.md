# Uruchomienie sklepu
```bash
npm install
npm run start
```
# API Sklepu
```
GET /bd/nazwa_sklepu/
```
Zwraca tablicą z całą zawartością sklepu 
```
GET /db/nazwa_sklepu/:id
```
Zwraca pojedynczy artykuł o id
```
POST /:id
```
Tworzy w bazie nowy artykuł o id, gdzie zawartość body to json:
```json
{count: number, name: string, price: number}
```
```
DELETE /:id
```
Usuwa z bazy artykuł o id
```
PUT /:id
```
Aktualizuje dane artukułu o id, gdzie body to json
```json
{count: number, name: string, price: number}
```
```
PUT /:id/buy
```
Zmniejsza zawartość stanu magazynowego o count, gdzie body to json:
```json
{count: number}
```
