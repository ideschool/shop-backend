const express = require('express');
const multer = require('multer');
const bodyParser = require('body-parser');
const jsonParser = bodyParser.json();
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'images')
    },
    filename: (req, file, cb) => {
        cb(null, file.originalname);
    }
});
const upload = multer({ storage: storage });

class Routes {
    constructor() {
        this.router = express.Router();
        this.routes();
    }

    routes() {
        this.router.post('/:id/picture',upload.single('picture'), this._uploadRoute.bind(this));
        this.router.post('/:id', jsonParser, this._postRoute.bind(this));
        this.router.put('/:id', jsonParser, this._putRoute.bind(this));
        this.router.put('/:id/buy', jsonParser, this._buyRoute.bind(this));
        this.router.get('/', this._getAllRoute.bind(this));
        this.router.get('/:id', this._getRoute.bind(this));
        this.router.delete('/:id', this._deleteRoute.bind(this));
    }
    _deleteRoute(req, res) {
        req.db.get(req.params.id).then((doc) => {
            console.log(doc);
            return req.db.remove(doc);
        }).then(() => {
            res.send({result: 'ok'});
        }).catch(function (error) {
            console.log(error);
            res.status(error.status).send(error.message);
        });
    }
    _uploadRoute(req, res) {
        req.db.get(req.params.id).then((doc) => {
            const data = doc.data;
            data.picture = `static/images/${req.file.filename}`;
            return req.db.put({
                _id: doc._id,
                _rev: doc._rev,
                data,
            });
        }).then((result) => {
            res.send({result})
        }).catch(function (error) {
            res.status(error.status).send(error.message);
        });
    }
    _postRoute(req, res) {
        if (req.body === undefined || req.params.id === undefined || req.body.price === undefined || req.body.name === undefined || req.body.count === undefined) {
            res.status(400).send("Incorrect data - some fields are missing");
        } else {
            req.db.put({
                _id: req.params.id,
                data: req.body,
            })
                .then((result) => {
                    res.send({result})
                }).catch((error) => {
                res.status(error.status).send(error.message);
            });
        }
    }

    _putRoute(req, res) {
        req.db.get(req.params.id).then((doc) => {
            return req.db.put({
                _id: doc._id,
                _rev: doc._rev,
                data: req.body,
            });
        }).then((result) => {
            res.send({result})
        }).catch(function (error) {
            res.status(error.status).send(error.message);
        });
    }

    _buyRoute(req, res) {
        req.db.get(req.params.id).then((doc) => {
            const data = doc.data;
            data.count = doc.data.count - req.body.count;
            return req.db.put({
                _id: req.params.id,
                _rev: doc._rev,
                data,
            });
        }).then((result) => {
            res.send({result})
        }).catch(function (error) {
            res.status(error.status).send(error.message);
        });
    }

    _getAllRoute(req, res) {
        req.db.allDocs({include_docs: true})
            .then((result) => {
                const data = result.rows.map((item) => item.doc);
                res.send(data);
            });
    }

    _getRoute(req, res) {
        req.db.get(req.params.id)
            .then((result) => {
                res.send({result})
            })
            .catch((error) => {
                res.status(error.status).send(error.message);
            });
    }
}

module.exports = Routes;
