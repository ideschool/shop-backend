const express = require('express');
const Routes = require('./routes');
const ShopDB = require('../shop-db');

class AppRouter {
    constructor() {
        this.router = express.Router();
        this.dbHelper = new ShopDB();
        this.routes();
    }

    routes() {
        const routes = new Routes();
        this.router.use('/:name', this.dbMiddleware.bind(this), routes.router);
    }

    dbMiddleware(req, res, next) {
        const name = req.params.name;
        req.db = this.dbHelper.getDatabase(name);
        next();
    }
}

module.exports = AppRouter;
