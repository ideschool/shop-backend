const PouchDB = require('pouchdb');
const fs = require('fs');
const dbPath = './db';

class ShopDB {
    constructor() {
        this._checkDbDir();
        this.databasesMap = new Map();
    }
    _checkDbDir() {
        if (!fs.existsSync(dbPath)){
            fs.mkdirSync(dbPath);
            console.log('db folder has been created');
        }
    }
    createDB(name) {
        const db = new PouchDB('./db/' + name);
        this.databasesMap.set(name, db);
        return db;
    };

    getDatabase(name) {
        return this.databasesMap.has(name) ? this.databasesMap.get(name) : this.createDB(name);
    }
}

module.exports = ShopDB;
